import './App.css';
import React from 'react';

function AttendeesList (props) {
    if (props.attendees === undefined) {
        return null;
        }
    return (
        <React.Fragment>
        <div className="container">
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Conference</th>
                </tr>
                </thead>
                <tbody>
                {props.attendees.map(attendee => {
                    return (
                    <tr key={attendee.href}>
                        <td>{ attendee.name }</td>
                        <td>{ attendee.conference }</td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
        </React.Fragment>
    );
}

export default AttendeesList;
